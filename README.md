# finwo / DMP

Distributed message passing platform

---

### What is dmp

DMP is a library to deliver string (null-terminated) messages across a peer-to-peer network in a pub/sub manner.

### How does it work

Before transmitting a message, a probe is sent by the publisher throughout the network containing the channel name to discover which nodes are subscribed to the channel.
Each subscriber will answer (echo) through the same path the probe went. Each echo received by the publisher will be answered with the actual message.

The publisher decides how many subscribers will receive the message and the timeout before the message is lost.

### What does it not do

- Encrypt your message
- Handle binary data

### Contributing

After checking the [GitHub issues](https://github.com/finwo/dmp/issues) and confirming that your request isn't already being worked on, feel free to spawn a new fork of the develop branch & send in a pull request.

The develop branch is merged periodically into the master after confirming it's stable, to make sure the master always contains a production-ready version.
